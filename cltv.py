# Churn Prediction Using XGBoost
from pyhive import hive
import pandas as pd
import numpy as np

# Config
hive_host = "cdsw.jordan.arabbank.plc"
hive_db = "insights_data"

# Connect to Hive Database
conn = hive.Connection(host=hive_host, database=hive_db)

customer_product = pd.read_sql("Select * from insights_data_nov.customer_product", conn)

product_balances = pd.read_sql("Select * from insights_data_nov.product_balances", conn)

product_balances['dim_date_id'] = pd.to_datetime(product_balances['dim_date_id'], format='%Y%m%d')
product_balances['year_month'] = product_balances['dim_date_id'].map(lambda dt: datetime.strftime(dt, '%Y-%B'))
product_balances['quarter'] = "Q%s_%s" %  (product_balances['dim_date_id'].dt.quarter, product_balances['dim_date_id'].dt.year)
unique_quarters = sorted(product_balances['quarter'].unique().tolist())
quater_weight = {q:w+1 for w, q in enumerate(unique_quarters)}
sum_weights = sum(quater_weight.values())
product_balances["weight"]  = product_balances['quarter'].replace(quater_weight)
product_balances["weighted_sum"] = product_balances["closing_balance_usd"]  * product_balances["weight"] 

merged = pd.merge(customer_product, product_balances, on='dim_account_id')

merged_grouped = merged.groupby('dim_customer_id')

data = []
for cust_id, group in merged_grouped:
	ca_rank = group[group["product_name"] == "CURRENT ACCOUNT"]["closing_balance_usd"].sum()/sum_weights
	sa_rank = group[group["product_name"] == "SAVING ACCOUNT"]["closing_balance_usd"].sum()/sum_weights
	td_rank = group[group["product_name"] == "CREDIT CARDS"]["closing_balance_usd"].sum()/sum_weights
	data.append([cust_id, max([ca_rank, sa_rank, td_rank])])

final_df = pd.DataFrame(data, columns=["dim_customer_id", "max_rank"])
final_df.to_csv("customer_ranks.csv")

